FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
#FROM mcr.microsoft.com/dotnet/aspnet:3.1-alpine AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
#FROM mcr.microsoft.com/dotnet/sdk:3.1-alpine AS build
WORKDIR /build
COPY ["src/Desafio.Api/Desafio.Api.csproj", "src/Desafio.Api/"]
COPY ["src/Desafio.Data/Desafio.Data.csproj", "src/Desafio.Data/"]
COPY ["src/Desafio.Domain/Desafio.Domain.csproj", "src/Desafio.Domain/"]
COPY ["src/Desafio.Service/Desafio.Service.csproj", "src/Desafio.Service/"]
RUN dotnet restore "src/Desafio.Api/Desafio.Api.csproj"
COPY . .
WORKDIR "/build/src/Desafio.Api"
RUN dotnet build "Desafio.Api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Desafio.Api.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Desafio.Api.dll"]