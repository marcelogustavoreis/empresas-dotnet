﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Security.Claims;

namespace Desafio.Api.Extensions
{
    public class RoleAuthorizeAttribute : TypeFilterAttribute
    {
        public RoleAuthorizeAttribute(string role) : base(typeof(CustomRoleAuthorizationFilter))
        {
            Arguments = new object[] { role };
        }
    }

    public class CustomRoleAuthorizationFilter : IAuthorizationFilter
    {
        private readonly string _role;

        public CustomRoleAuthorizationFilter(string role)
        {
            _role = role;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (!context.HttpContext.User.Identity.IsAuthenticated)
            {
                context.Result = new StatusCodeResult(401);
                return;
            }

            if (!RoleAuthorize.ValidarRoleUsuario(context.HttpContext, _role))
            {
                context.Result = new StatusCodeResult(403);
            }
        }
    }

    public class RoleAuthorize
    {
        public static bool ValidarRoleUsuario(HttpContext context, string role)
        {
            return context.User.Identity.IsAuthenticated &&
                   context.User.IsInRole(role);
        }
    }
}
