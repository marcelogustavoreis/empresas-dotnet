﻿using Desafio.Api.Model;
using Desafio.Data;
using Desafio.Data.Repository;
using Desafio.Data.Seed;
using Desafio.Domain.Interface;
using Desafio.Domain.Interface.Repository;
using Desafio.Domain.Interface.Service;
using Desafio.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Desafio.Api.ApiConfiguration
{
    public static class DependencyInjectionConfig
    {
        public static IServiceCollection ResolveDependencies(this IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IAspNetUser, AspNetUser>();

            services.AddScoped<IApplicationUserRepository, ApplicationUserRepository>();
            services.AddScoped<IFilmeRepository, FilmeRepository>();

            services.AddScoped<IFilmeService, FilmeService>();

            services.AddTransient<ConfigurarInicializacaoBancoDados>();
            services.AddTransient<RolesSeedData>();
            services.AddTransient<DefaultAdminUserSeed>();

            return services;
        }
    }
}
