﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Desafio.Api.ApiConfiguration;
using Desafio.Api.Extensions;
using Desafio.Api.Model;
using Desafio.Domain.Dto;
using Desafio.Domain.Entity;
using Desafio.Domain.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Desafio.Api.Controllers
{
    [Route("api/usuario")]
    public class UsuarioController : MainController
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly JwtSettings _jwtSettings;
        private readonly IAspNetUser _user;

        public UsuarioController(SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager,
            IOptions<JwtSettings> jwtSettings, IAspNetUser user)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _jwtSettings = jwtSettings.Value;
            _user = user;
        }

        [HttpPost("cadastro")]
        [AllowAnonymous]
        public async Task<IActionResult> Cadastro(UsuarioCadastroModel usuarioCadastroModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var user = new ApplicationUser(usuarioCadastroModel.NomeCompleto, usuarioCadastroModel.Email);

            var result = await _userManager.CreateAsync(user, usuarioCadastroModel.Senha);

            if (result.Succeeded)
            {
                return CustomResponse();
            }

            foreach (var error in result.Errors)
            {
                AdicionarErroProcessamento(error.Description);
            }

            return CustomResponse();
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<ActionResult> Login(UsuarioLoginModel usuarioLoginModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var usuario = await _userManager.FindByEmailAsync(usuarioLoginModel.Email);
            if (!usuario.EstaAtivo)
            {
                AdicionarErroProcessamento("Usuário foi desativado");
                return CustomResponse();
            }

            var resposta = await _signInManager.PasswordSignInAsync(usuarioLoginModel.Email, usuarioLoginModel.Senha,
                false, true);

            if (resposta.Succeeded)
            {
                return CustomResponse(await GerarJwt(usuarioLoginModel.Email));
            }

            if (resposta.IsLockedOut)
            {
                AdicionarErroProcessamento("Usuário temporariamente bloqueado por tentativas inválidas");
                return CustomResponse();
            }

            AdicionarErroProcessamento("Usuário ou Senha incorretos");
            return CustomResponse();
        }

        [HttpPut]
        public async Task<IActionResult> Update(UsuarioEditarModel usuarioEditarModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var usuarioCadastrado = await _userManager.FindByIdAsync(_user.IdUsuario.ToString());
            if (usuarioCadastrado == null)
            {
                AdicionarErroProcessamento("Usuário não encontrado para edição");
                return CustomResponse();
            }
            usuarioCadastrado.Atualizar(usuarioEditarModel.NomeCompleto);
            var result = await _userManager.UpdateAsync(usuarioCadastrado);

            if (result.Succeeded)
            {
                return CustomResponse();
            }

            foreach (var error in result.Errors)
            {
                AdicionarErroProcessamento(error.Description);
            }

            return CustomResponse();
        }

        [HttpPut("alterar-senha")]
        public async Task<IActionResult> TrocarSenha(UsuarioAlterarSenhaModel usuarioAlterarSenhaModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var usuarioCadastrado = await _userManager.FindByIdAsync(_user.IdUsuario.ToString());
            if (usuarioCadastrado == null)
            {
                AdicionarErroProcessamento("Usuário não encontrado para trocar senha");
                return CustomResponse();
            }
            var result = await _userManager.ChangePasswordAsync(usuarioCadastrado, usuarioAlterarSenhaModel.SenhaAtual, usuarioAlterarSenhaModel.NovaSenha);

            if (result.Succeeded)
            {
                return CustomResponse();
            }

            foreach (var error in result.Errors)
            {
                AdicionarErroProcessamento(error.Description);
            }

            return CustomResponse();
        }

        [HttpPut("desativar")]
        public async Task<IActionResult> Desativar()
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var usuarioCadastrado = await _userManager.FindByIdAsync(_user.IdUsuario.ToString());
            if (usuarioCadastrado == null)
            {
                AdicionarErroProcessamento("Usuário não encontrado para desativar");
                return CustomResponse();
            }

            usuarioCadastrado.Desativar();
            var result = await _userManager.UpdateAsync(usuarioCadastrado);

            if (result.Succeeded)
            {
                return CustomResponse();
            }

            foreach (var error in result.Errors)
            {
                AdicionarErroProcessamento(error.Description);
            }

            return CustomResponse();
        }

        private async Task<UsuarioRespostaLoginModel> GerarJwt(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);
            var claims = await _userManager.GetClaimsAsync(user);

            var identityClaims = await ObterClaimsUsuario(claims, user);
            var encodedToken = CodificarToken(identityClaims);

            return new UsuarioRespostaLoginModel
            {
                AccessToken = encodedToken,
                ExpiresIn = TimeSpan.FromHours(_jwtSettings.ExpiracaoEmMinutos).TotalSeconds,
            };
        }

        private async Task<ClaimsIdentity> ObterClaimsUsuario(ICollection<Claim> claims, ApplicationUser user)
        {
            var userRoles = await _userManager.GetRolesAsync(user);

            claims.Add(new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()));
            claims.Add(new Claim(JwtRegisteredClaimNames.Email, user.Email));
            claims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            claims.Add(new Claim(JwtRegisteredClaimNames.Nbf, DateTime.UtcNow.ToUnixEpochDate()));
            claims.Add(new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToUnixEpochDate(), ClaimValueTypes.Integer64));
            foreach (var userRole in userRoles)
            {
                claims.Add(new Claim("role", userRole));
            }

            var identityClaims = new ClaimsIdentity();
            identityClaims.AddClaims(claims);

            return identityClaims;
        }

        private string CodificarToken(ClaimsIdentity identityClaims)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_jwtSettings.Secret);
            var token = tokenHandler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = _jwtSettings.Issuer,
                Audience = _jwtSettings.Audience,
                Subject = identityClaims,
                Expires = DateTime.UtcNow.AddMinutes(_jwtSettings.ExpiracaoEmMinutos),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            });

            return tokenHandler.WriteToken(token);
        }
    }
}
