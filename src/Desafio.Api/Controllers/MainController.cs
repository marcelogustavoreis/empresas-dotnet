﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;
using System.Linq;

namespace Desafio.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public abstract class MainController : ControllerBase
    {
        private readonly ICollection<string> _erros = new List<string>();

        protected ActionResult CustomResponse(object result = null)
        {
            if (OperacaoValida())
            {
                if (result == null)
                    return Ok();

                return Ok(result);
            }

            return BadRequest(new ValidationProblemDetails(new Dictionary<string, string[]>
            {
                { "Mensagens", _erros.ToArray() }
            }));
        }

        protected ActionResult CustomResponse(ModelStateDictionary modelState)
        {
            var erros = modelState.Values.SelectMany(e => e.Errors);
            foreach (var erro in erros)
            {
                AdicionarErroProcessamento(erro.ErrorMessage);
            }

            return CustomResponse();
        }

        protected bool OperacaoValida()
        {
            return !_erros.Any();
        }

        protected void AdicionarErroProcessamento(string erro)
        {
            _erros.Add(erro);
        }

        protected void LimparErrosProcessamento()
        {
            _erros.Clear();
        }
    }
}
