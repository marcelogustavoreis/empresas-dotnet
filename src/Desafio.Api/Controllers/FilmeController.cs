﻿using System;
using System.Threading.Tasks;
using Desafio.Api.Extensions;
using Desafio.Api.Model;
using Desafio.Data.Helper;
using Desafio.Domain;
using Desafio.Domain.Entity;
using Desafio.Domain.Filtros;
using Desafio.Domain.Interface;
using Desafio.Domain.Interface.Repository;
using Desafio.Domain.Interface.Service;
using Microsoft.AspNetCore.Mvc;

namespace Desafio.Api.Controllers
{
    [Route("api/filme")]
    public class FilmeController : MainController
    {
        private readonly IAspNetUser _user;
        private readonly IFilmeService _filmeService;
        private readonly IFilmeRepository _filmeRepository;

        public FilmeController(IAspNetUser user, IFilmeService filmeService, IFilmeRepository filmeRepository)
        {
            _user = user;
            _filmeService = filmeService;
            _filmeRepository = filmeRepository;
        }

        [RoleAuthorize(RoleNames.Admin)]
        [HttpPost]
        public async Task<IActionResult> Cadastro(FilmeCadastroModel model)
        {
            var filme = new Filme(model.Titulo, model.Sinopse);
            await _filmeService.Cadastro(filme, model.Atores, model.Diretores, model.Generos);

            return CustomResponse();
        }

        [HttpGet]
        public async Task<IActionResult> Listar([FromQuery] FilmeFiltro filtro)
        {
            var filmes = await _filmeRepository.ListarAsync(filtro);
            return CustomResponse(filmes);
        }

        [HttpPost("voto/{filmeId:guid}")]
        public async Task<IActionResult> VotoFilme(Guid filmeId, FilmeVotoModel model)
        {
            try
            {
                await _filmeService.AdicionarVotoFilme(filmeId, model.Nota);
            }
            catch (DomainException ex)
            {
                AdicionarErroProcessamento(ex.Message);
                return CustomResponse();
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return CustomResponse();
        }

        [HttpGet("detalhe/{filmeId:guid}")]
        public async Task<IActionResult> DetalheFilme(Guid filmeId)
        {
            var filme = await _filmeRepository.DetalhesAsync(filmeId);

            return CustomResponse(filme);
        }
    }
}
