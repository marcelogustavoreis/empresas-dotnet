﻿using System.Threading.Tasks;
using Desafio.Api.Extensions;
using Desafio.Api.Model;
using Desafio.Data.Helper;
using Desafio.Domain.Entity;
using Desafio.Domain.Filtros;
using Desafio.Domain.Interface;
using Desafio.Domain.Interface.Repository;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Desafio.Api.Controllers
{
    [Route("api/administrador")]
    public class AdministradorController : MainController
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IAspNetUser _user;
        private readonly IApplicationUserRepository _applicationUserRepository;

        public AdministradorController(UserManager<ApplicationUser> userManager, IAspNetUser user,
            IApplicationUserRepository applicationUserRepository)
        {
            _userManager = userManager;
            _user = user;
            _applicationUserRepository = applicationUserRepository;
        }

        [RoleAuthorize(RoleNames.Admin)]
        [HttpPost]
        public async Task<IActionResult> Cadastro(UsuarioCadastroModel usuarioCadastroModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var user = new ApplicationUser(usuarioCadastroModel.NomeCompleto, usuarioCadastroModel.Email);

            var result = await _userManager.CreateAsync(user, usuarioCadastroModel.Senha);

            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, RoleNames.Admin);

                return CustomResponse();
            }

            foreach (var error in result.Errors)
            {
                AdicionarErroProcessamento(error.Description);
            }

            return CustomResponse();
        }

        [RoleAuthorize(RoleNames.Admin)]
        [HttpPut]
        public async Task<IActionResult> Update(UsuarioEditarModel usuarioEditarModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var usuarioCadastrado = await _userManager.FindByIdAsync(_user.IdUsuario.ToString());
            if (usuarioCadastrado == null)
            {
                AdicionarErroProcessamento("Usuário não encontrado para edição");
                return CustomResponse();
            }
            usuarioCadastrado.Atualizar(usuarioEditarModel.NomeCompleto);
            var result = await _userManager.UpdateAsync(usuarioCadastrado);

            if (result.Succeeded)
            {
                return CustomResponse();
            }

            foreach (var error in result.Errors)
            {
                AdicionarErroProcessamento(error.Description);
            }

            return CustomResponse();
        }

        [RoleAuthorize(RoleNames.Admin)]
        [HttpPut("alterar-senha")]
        public async Task<IActionResult> TrocarSenha(UsuarioAlterarSenhaModel usuarioAlterarSenhaModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var usuarioCadastrado = await _userManager.FindByIdAsync(_user.IdUsuario.ToString());
            if (usuarioCadastrado == null)
            {
                AdicionarErroProcessamento("Usuário não encontrado para trocar senha");
                return CustomResponse();
            }
            var result = await _userManager.ChangePasswordAsync(usuarioCadastrado, usuarioAlterarSenhaModel.SenhaAtual, usuarioAlterarSenhaModel.NovaSenha);

            if (result.Succeeded)
            {
                return CustomResponse();
            }

            foreach (var error in result.Errors)
            {
                AdicionarErroProcessamento(error.Description);
            }

            return CustomResponse();
        }

        [RoleAuthorize(RoleNames.Admin)]
        [HttpPut("desativar")]
        public async Task<IActionResult> Desativar()
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var usuarioCadastrado = await _userManager.FindByIdAsync(_user.IdUsuario.ToString());
            if (usuarioCadastrado == null)
            {
                AdicionarErroProcessamento("Usuário não encontrado para desativar");
                return CustomResponse();
            }

            usuarioCadastrado.Desativar();
            var result = await _userManager.UpdateAsync(usuarioCadastrado);

            if (result.Succeeded)
            {
                return CustomResponse();
            }

            foreach (var error in result.Errors)
            {
                AdicionarErroProcessamento(error.Description);
            }

            return CustomResponse();
        }

        [RoleAuthorize(RoleNames.Admin)]
        [HttpGet("listar-usuarios-ativos")]
        public async Task<IActionResult> ListarUsuarios([FromQuery] ApplicationUserFiltro filtro)
        {
            var usuarios = await _applicationUserRepository.ListarNaoAdministradoresAtivosAsync(filtro);

            return CustomResponse(usuarios);
        }
    }
}
