﻿using System.ComponentModel.DataAnnotations;

namespace Desafio.Api.Model
{
    public class FilmeCadastroModel
    {
        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        [Display(Name = "Título")]
        public string Titulo { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public string Sinopse { get; set; }

        public string[] Atores { get; set; }
        public string[] Diretores { get; set; }
        public string[] Generos { get; set; }
    }

    public class FilmeVotoModel
    {
        [Range(0, 4, ErrorMessage = "O campo {0} precisa ser no mínimo {1} e no máximo {2}")]
        public int Nota { get; set; }
    }
}
