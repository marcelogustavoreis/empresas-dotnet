﻿using Desafio.Domain;
using Desafio.Domain.Entity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Desafio.Data
{
    public class ApplicationDbContext : DbContext, IUnitOfWork
    {
        public DbSet<Filme> Filmes { get; set; }
        public DbSet<Ator> Atores { get; set; }
        public DbSet<Diretor> Diretores { get; set; }
        public DbSet<Genero> Generos { get; set; }

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            foreach (var property in modelBuilder.Model.GetEntityTypes().SelectMany(
                e => e.GetProperties().Where(p => p.ClrType == typeof(string))))
                property.SetColumnType("varchar(100)");

            foreach (var relationship in modelBuilder.Model.GetEntityTypes()
                .SelectMany(e => e.GetForeignKeys())) relationship.DeleteBehavior = DeleteBehavior.ClientSetNull;

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ApplicationDbContext).Assembly);

            modelBuilder.Entity<ApplicationUser>(builder =>
            {
                builder.Property(c => c.EstaAtivo)
                .IsRequired();

                builder.Property(c => c.NomeCompleto)
                    .IsRequired()
                    .HasColumnType("varchar(100)");

                builder.ToTable("AspNetUsers");
            });

            modelBuilder.Entity<ApplicationUserRole>(builder =>
            {
                builder.HasKey(e => new { e.UserId, e.RoleId });

                builder.HasOne(d => d.Role)
                    .WithMany(p => p.UserRoles)
                    .HasForeignKey(d => d.RoleId);

                builder.HasOne(d => d.User)
                    .WithMany(p => p.UserRoles)
                    .HasForeignKey(d => d.UserId);

                builder.ToTable("AspNetUserRoles");
            });

            modelBuilder.Entity<ApplicationRole>(builder =>
            {
                builder.HasIndex(e => e.NormalizedName, "RoleNameIndex")
                    .IsUnique()
                    .HasFilter("([NormalizedName] IS NOT NULL)");

                builder.Property(e => e.Name).HasMaxLength(256);

                builder.Property(e => e.NormalizedName).HasMaxLength(256);

                builder.ToTable("AspNetRoles");
            });

            modelBuilder.Entity<ApplicationUser>().ToTable("AspNetUsers", t => t.ExcludeFromMigrations());
            modelBuilder.Entity<ApplicationUserRole>().ToTable("AspNetUserRoles", t => t.ExcludeFromMigrations());
            modelBuilder.Entity<ApplicationRole>().ToTable("AspNetRoles", t => t.ExcludeFromMigrations());
        }

        public async Task<bool> Commit()
        {
            return await base.SaveChangesAsync() > 0;
        }
    }
}
