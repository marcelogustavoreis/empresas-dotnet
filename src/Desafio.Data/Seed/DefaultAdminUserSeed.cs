﻿using Desafio.Data.Helper;
using Desafio.Domain.Entity;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace Desafio.Data.Seed
{
    public class DefaultAdminUserSeed
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RolesSeedData _rolesSeedData;

        public DefaultAdminUserSeed(UserManager<ApplicationUser> userManager, RolesSeedData rolesSeedData)
        {
            _userManager = userManager;
            _rolesSeedData = rolesSeedData;
        }

        public async Task Seed()
        {
            await _rolesSeedData.Seed();

            string email = "admin@admin.com";
            string senha = "Teste@123";

            var usuarioExistente = await _userManager.FindByEmailAsync(email);
            if (usuarioExistente != null) return;

            var user = new ApplicationUser("Administrador", email);
            var result = await _userManager.CreateAsync(user, senha);
            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, RoleNames.Admin);
            }
        }
    }
}
