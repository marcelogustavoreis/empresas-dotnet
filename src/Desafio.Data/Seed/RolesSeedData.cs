﻿using Desafio.Data.Helper;
using Desafio.Domain.Entity;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Desafio.Data.Seed
{
    public class RolesSeedData
    {
        private readonly RoleManager<ApplicationRole> _roleManager;

        public RolesSeedData(RoleManager<ApplicationRole> roleManager)
        {
            _roleManager = roleManager;
        }

        public async Task Seed()
        {
            string[] roles = new string[] { RoleNames.Admin };

            foreach (var roleName in roles)
            {
                var role = await _roleManager.FindByNameAsync(roleName);
                if (role == null)
                {
                    await _roleManager.CreateAsync(new ApplicationRole(roleName));
                }
            }
        }
    }
}
