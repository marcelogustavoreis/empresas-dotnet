﻿using Desafio.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Pluto.Carteiras.Api.Data.Mappings
{
    public class VotoFilmeMapping : IEntityTypeConfiguration<VotoFilme>
    {
        public void Configure(EntityTypeBuilder<VotoFilme> builder)
        {
            builder.HasKey(c => new { c.UsuarioId, c.FilmeId });

            builder.Property(c => c.Nota)
                .IsRequired();

            builder.HasOne(c => c.Filme);
            builder.HasOne(c => c.Usuario);

            builder.ToTable("VotosFilmes");
        }
    }
}
