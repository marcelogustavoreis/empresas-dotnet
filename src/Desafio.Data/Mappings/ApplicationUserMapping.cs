﻿using Desafio.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Pluto.Carteiras.Api.Data.Mappings
{
    public class ApplicationUserMapping : IEntityTypeConfiguration<ApplicationUser>
    {
        public void Configure(EntityTypeBuilder<ApplicationUser> builder)
        {
            builder.Property(c => c.EstaAtivo)
                    .IsRequired();

            builder.Property(c => c.NomeCompleto)
                .IsRequired()
                .HasColumnType("varchar(100)");

            builder.ToTable("AspNetUsers");
        }
    }

}
