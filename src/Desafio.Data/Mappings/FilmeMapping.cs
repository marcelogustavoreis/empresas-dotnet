﻿using Desafio.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Pluto.Carteiras.Api.Data.Mappings
{
    public class FilmeMapping : IEntityTypeConfiguration<Filme>
    {
        public void Configure(EntityTypeBuilder<Filme> builder)
        {
            builder.Property(c => c.Id).ValueGeneratedNever();
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Titulo)
                .IsRequired()
                .HasColumnType("varchar(100)");

            builder.Property(c => c.Sinopse)
                .IsRequired()
                .HasColumnType("varchar(500)");

            builder.HasMany(c => c.Atores)
                .WithMany(c => c.Filmes)
                .UsingEntity(c => c.ToTable("AtoresFilmes"));

            builder.HasMany(c => c.Diretores)
                .WithMany(c => c.Filmes)
                .UsingEntity(c => c.ToTable("DiretoresFilmes"));

            builder.HasMany(c => c.Generos)
                .WithMany(c => c.Filmes)
                .UsingEntity(c => c.ToTable("GenerosFilmes"));

            builder.HasMany(c => c.Votos)
                .WithOne(c => c.Filme);

            builder.ToTable("Filmes");
        }
    }
}
