﻿using Desafio.Data.Seed;
using Desafio.Domain;
using Desafio.Domain.Entity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Desafio.Data
{
    public class MyIdentityDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, Guid>, IUnitOfWork
    {
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }

        public MyIdentityDbContext(DbContextOptions<MyIdentityDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationUser>(builder =>
            {
                builder.Property(c => c.EstaAtivo)
                    .IsRequired();

                builder.Property(c => c.NomeCompleto)
                    .IsRequired()
                    .HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<ApplicationUserRole>(builder =>
            {
                builder.HasOne(d => d.Role)
                    .WithMany(p => p.UserRoles)
                    .HasForeignKey(d => d.RoleId);

                builder.HasOne(d => d.User)
                    .WithMany(p => p.UserRoles)
                    .HasForeignKey(d => d.UserId);

                builder.ToTable("AspNetUserRoles");
            });

            modelBuilder.Entity<ApplicationRole>(builder =>
            {
                builder.HasIndex(e => e.NormalizedName, "RoleNameIndex")
                    .IsUnique()
                    .HasFilter("([NormalizedName] IS NOT NULL)");

                builder.Property(e => e.Name).HasMaxLength(256);

                builder.Property(e => e.NormalizedName).HasMaxLength(256);

                builder.ToTable("AspNetRoles");
            });

            modelBuilder.UseIdentityColumns();
        }

        public async Task<bool> Commit()
        {
            return await base.SaveChangesAsync() > 0;
        }
    }
}
