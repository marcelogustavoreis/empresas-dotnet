﻿using Desafio.Domain;
using Desafio.Domain.Interface.Repository;

namespace Desafio.Data.Repository
{
    public abstract class BaseRepository : IBaseRepository
    {
        protected readonly ApplicationDbContext _context;
        public BaseRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IUnitOfWork UnitOfWork => _context;

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
