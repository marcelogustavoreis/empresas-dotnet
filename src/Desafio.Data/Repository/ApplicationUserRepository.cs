﻿using Desafio.Data.Helper;
using Desafio.Domain.Dto;
using Desafio.Domain.Entity;
using Desafio.Domain.Filtros;
using Desafio.Domain.Interface.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desafio.Data.Repository
{
    public class ApplicationUserRepository : BaseRepository, IApplicationUserRepository
    {
        public ApplicationUserRepository(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<ListaPaginada<UsuarioAtivoDto>> ListarNaoAdministradoresAtivosAsync(ApplicationUserFiltro filtro)
        {
            var applicationUser = _context.ApplicationUsers
                .AsNoTracking()
                .Include(x => x.UserRoles)
                .ThenInclude(x => x.Role)
                .Where(x => !x.UserRoles.Any(y => y.Role.Name == RoleNames.Admin) && x.EstaAtivo)
                .OrderBy(x => x.NomeCompleto)
                .Select(x => new UsuarioAtivoDto()
                {
                    NomeCompleto = x.NomeCompleto,
                    Email = x.Email
                });

            return await ListaPaginada<UsuarioAtivoDto>.CreateAsync(applicationUser, filtro.Pagina, filtro.QuantidadeItensPagina);
        }
    }
}
