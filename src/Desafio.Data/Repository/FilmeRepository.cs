﻿using Desafio.Domain.Dto;
using Desafio.Domain.Entity;
using Desafio.Domain.Filtros;
using Desafio.Domain.Interface.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desafio.Data.Repository
{
    public class FilmeRepository : BaseRepository, IFilmeRepository
    {
        public FilmeRepository(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<Filme> CadastrarAsync(Filme filme)
        {
            await _context.Filmes.AddAsync(filme);
            await _context.Commit();

            return filme;
        }

        public async Task<ListaPaginada<ListagemFilmeDto>> ListarAsync(FilmeFiltro filtros)
        {
            var filmes = _context.Filmes
                .AsNoTracking()
                .Include(x => x.Atores)
                .Include(x => x.Diretores)
                .Include(x => x.Generos)
                .Include(x => x.Votos)
                .AsQueryable();

            if (!string.IsNullOrWhiteSpace(filtros.Ator))
            {
                filmes = filmes.Where(x => x.Atores.Any(y => y.Nome == filtros.Ator));
            }

            if (!string.IsNullOrWhiteSpace(filtros.Diretor))
            {
                filmes = filmes.Where(x => x.Diretores.Any(y => y.Nome == filtros.Diretor));
            }

            if (!string.IsNullOrWhiteSpace(filtros.Genero))
            {
                filmes = filmes.Where(x => x.Generos.Any(y => y.Descricao == filtros.Genero));
            }

            switch (filtros.Ordenacao)
            {
                case FilmeOrdenacao.MediaVotos:
                    filmes = filmes.OrderByDescending(x => x.Votos.Count == 0 ? 0 : x.Votos.Sum(y => y.Nota) / x.Votos.Count);
                    break;
                case FilmeOrdenacao.Titulo:
                default:
                    filmes = filmes.OrderBy(x => x.Titulo);
                    break;
            }

            var filmesDto = filmes.Select(x => new ListagemFilmeDto()
            {
                Id = x.Id,
                Sinopse = x.Sinopse,
                Titulo = x.Titulo,
                Atores = string.Join(", ", x.Atores.Select(y=>y.Nome)),
                Diretores = string.Join(", ", x.Diretores.Select(y => y.Nome)),
                Generos = string.Join(", ", x.Generos.Select(y => y.Descricao)),
            });

            return await ListaPaginada<ListagemFilmeDto>.CreateAsync(filmesDto, filtros.Pagina, filtros.QuantidadeItensPagina);
        }

        public async Task<DetalheFilmeDto> DetalhesAsync(Guid filmeId)
        {
            return await _context.Filmes
                .AsNoTracking()
                .Include(x => x.Atores)
                .Include(x => x.Diretores)
                .Include(x => x.Generos)
                .Include(x => x.Votos)
                .Where(x => x.Id == filmeId)
                .Select(x => new DetalheFilmeDto()
                {
                    Id = x.Id,
                    Sinopse = x.Sinopse,
                    Titulo = x.Titulo,
                    MediaVotos = x.MediaVotos,
                    Atores = string.Join(", ", x.Atores.Select(y => y.Nome)),
                    Diretores = string.Join(", ", x.Diretores.Select(y => y.Nome)),
                    Generos = string.Join(", ", x.Generos.Select(y => y.Descricao)),
                })
                .FirstOrDefaultAsync();
        }

        public async Task<Ator> ObterAtorPorNome(string nome)
        {
            return await _context.Atores.FirstOrDefaultAsync(x => x.Nome == nome);
        }

        public async Task<Diretor> ObterDiretorPorNome(string nome)
        {
            return await _context.Diretores.FirstOrDefaultAsync(x => x.Nome == nome);
        }

        public async Task<Genero> ObterGeneroPorDescricao(string descricao)
        {
            return await _context.Generos.FirstOrDefaultAsync(x => x.Descricao == descricao);
        }

        public async Task<Filme> ObterPorIdComVotosAsync(Guid id)
        {
            return await _context.Filmes
                .Include(x => x.Votos)
                .FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
