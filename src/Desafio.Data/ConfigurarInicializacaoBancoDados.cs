﻿using Desafio.Data.Seed;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Desafio.Data
{
    public class ConfigurarInicializacaoBancoDados
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly MyIdentityDbContext _myIdentityDbContext;
        private readonly DefaultAdminUserSeed _defaultAdminUserSeed;

        public ConfigurarInicializacaoBancoDados(ApplicationDbContext applicationDbContext, MyIdentityDbContext myIdentityDbContext,
            DefaultAdminUserSeed defaultAdminUserSeed)
        {
            _applicationDbContext = applicationDbContext;
            _myIdentityDbContext = myIdentityDbContext;
            _defaultAdminUserSeed = defaultAdminUserSeed;
        }

        public async Task Iniciar()
        {
            await _myIdentityDbContext.Database.MigrateAsync();
            await _applicationDbContext.Database.MigrateAsync();
            await _defaultAdminUserSeed.Seed();
        }
    }
}
