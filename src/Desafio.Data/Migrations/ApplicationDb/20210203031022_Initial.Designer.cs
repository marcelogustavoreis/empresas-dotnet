﻿// <auto-generated />
using System;
using Desafio.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Desafio.Data.Migrations.ApplicationDb
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20210203031022_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .UseIdentityColumns()
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.2");

            modelBuilder.Entity("AtorFilme", b =>
                {
                    b.Property<Guid>("AtoresId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("FilmesId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("AtoresId", "FilmesId");

                    b.HasIndex("FilmesId");

                    b.ToTable("AtoresFilmes");
                });

            modelBuilder.Entity("Desafio.Domain.Entity.ApplicationRole", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("ConcurrencyStamp")
                        .HasColumnType("varchar(100)");

                    b.Property<string>("Name")
                        .HasMaxLength(256)
                        .HasColumnType("varchar(256)");

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256)
                        .HasColumnType("varchar(256)");

                    b.HasKey("Id");

                    b.HasIndex(new[] { "NormalizedName" }, "RoleNameIndex")
                        .IsUnique()
                        .HasFilter("([NormalizedName] IS NOT NULL)");

                    b.ToTable("AspNetRoles", t => t.ExcludeFromMigrations());
                });

            modelBuilder.Entity("Desafio.Domain.Entity.ApplicationUser", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<int>("AccessFailedCount")
                        .HasColumnType("int");

                    b.Property<string>("ConcurrencyStamp")
                        .HasColumnType("varchar(100)");

                    b.Property<string>("Email")
                        .HasColumnType("varchar(100)");

                    b.Property<bool>("EmailConfirmed")
                        .HasColumnType("bit");

                    b.Property<bool>("EstaAtivo")
                        .HasColumnType("bit");

                    b.Property<bool>("LockoutEnabled")
                        .HasColumnType("bit");

                    b.Property<DateTimeOffset?>("LockoutEnd")
                        .HasColumnType("datetimeoffset");

                    b.Property<string>("NomeCompleto")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.Property<string>("NormalizedEmail")
                        .HasColumnType("varchar(100)");

                    b.Property<string>("NormalizedUserName")
                        .HasColumnType("varchar(100)");

                    b.Property<string>("PasswordHash")
                        .HasColumnType("varchar(100)");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("varchar(100)");

                    b.Property<bool>("PhoneNumberConfirmed")
                        .HasColumnType("bit");

                    b.Property<string>("SecurityStamp")
                        .HasColumnType("varchar(100)");

                    b.Property<bool>("TwoFactorEnabled")
                        .HasColumnType("bit");

                    b.Property<string>("UserName")
                        .HasColumnType("varchar(100)");

                    b.HasKey("Id");

                    b.ToTable("AspNetUsers", t => t.ExcludeFromMigrations());
                });

            modelBuilder.Entity("Desafio.Domain.Entity.ApplicationUserRole", b =>
                {
                    b.Property<Guid>("UserId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("RoleId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles", t => t.ExcludeFromMigrations());
                });

            modelBuilder.Entity("Desafio.Domain.Entity.Ator", b =>
                {
                    b.Property<Guid>("Id")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Nome")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.HasKey("Id");

                    b.ToTable("Atores");
                });

            modelBuilder.Entity("Desafio.Domain.Entity.Diretor", b =>
                {
                    b.Property<Guid>("Id")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Nome")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.HasKey("Id");

                    b.ToTable("Diretores");
                });

            modelBuilder.Entity("Desafio.Domain.Entity.Filme", b =>
                {
                    b.Property<Guid>("Id")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Sinopse")
                        .IsRequired()
                        .HasColumnType("varchar(500)");

                    b.Property<string>("Titulo")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.HasKey("Id");

                    b.ToTable("Filmes");
                });

            modelBuilder.Entity("Desafio.Domain.Entity.Genero", b =>
                {
                    b.Property<Guid>("Id")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Descricao")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.HasKey("Id");

                    b.ToTable("Generos");
                });

            modelBuilder.Entity("Desafio.Domain.Entity.VotoFilme", b =>
                {
                    b.Property<Guid>("UsuarioId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("FilmeId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<int>("Nota")
                        .HasColumnType("int");

                    b.HasKey("UsuarioId", "FilmeId");

                    b.HasIndex("FilmeId");

                    b.ToTable("VotosFilmes");
                });

            modelBuilder.Entity("DiretorFilme", b =>
                {
                    b.Property<Guid>("DiretoresId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("FilmesId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("DiretoresId", "FilmesId");

                    b.HasIndex("FilmesId");

                    b.ToTable("DiretoresFilmes");
                });

            modelBuilder.Entity("FilmeGenero", b =>
                {
                    b.Property<Guid>("FilmesId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("GenerosId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("FilmesId", "GenerosId");

                    b.HasIndex("GenerosId");

                    b.ToTable("GenerosFilmes");
                });

            modelBuilder.Entity("AtorFilme", b =>
                {
                    b.HasOne("Desafio.Domain.Entity.Ator", null)
                        .WithMany()
                        .HasForeignKey("AtoresId")
                        .IsRequired();

                    b.HasOne("Desafio.Domain.Entity.Filme", null)
                        .WithMany()
                        .HasForeignKey("FilmesId")
                        .IsRequired();
                });

            modelBuilder.Entity("Desafio.Domain.Entity.ApplicationUserRole", b =>
                {
                    b.HasOne("Desafio.Domain.Entity.ApplicationRole", "Role")
                        .WithMany("UserRoles")
                        .HasForeignKey("RoleId")
                        .IsRequired();

                    b.HasOne("Desafio.Domain.Entity.ApplicationUser", "User")
                        .WithMany("UserRoles")
                        .HasForeignKey("UserId")
                        .IsRequired();

                    b.Navigation("Role");

                    b.Navigation("User");
                });

            modelBuilder.Entity("Desafio.Domain.Entity.VotoFilme", b =>
                {
                    b.HasOne("Desafio.Domain.Entity.Filme", "Filme")
                        .WithMany("Votos")
                        .HasForeignKey("FilmeId")
                        .IsRequired();

                    b.HasOne("Desafio.Domain.Entity.ApplicationUser", "Usuario")
                        .WithMany()
                        .HasForeignKey("UsuarioId")
                        .IsRequired();

                    b.Navigation("Filme");

                    b.Navigation("Usuario");
                });

            modelBuilder.Entity("DiretorFilme", b =>
                {
                    b.HasOne("Desafio.Domain.Entity.Diretor", null)
                        .WithMany()
                        .HasForeignKey("DiretoresId")
                        .IsRequired();

                    b.HasOne("Desafio.Domain.Entity.Filme", null)
                        .WithMany()
                        .HasForeignKey("FilmesId")
                        .IsRequired();
                });

            modelBuilder.Entity("FilmeGenero", b =>
                {
                    b.HasOne("Desafio.Domain.Entity.Filme", null)
                        .WithMany()
                        .HasForeignKey("FilmesId")
                        .IsRequired();

                    b.HasOne("Desafio.Domain.Entity.Genero", null)
                        .WithMany()
                        .HasForeignKey("GenerosId")
                        .IsRequired();
                });

            modelBuilder.Entity("Desafio.Domain.Entity.ApplicationRole", b =>
                {
                    b.Navigation("UserRoles");
                });

            modelBuilder.Entity("Desafio.Domain.Entity.ApplicationUser", b =>
                {
                    b.Navigation("UserRoles");
                });

            modelBuilder.Entity("Desafio.Domain.Entity.Filme", b =>
                {
                    b.Navigation("Votos");
                });
#pragma warning restore 612, 618
        }
    }
}
