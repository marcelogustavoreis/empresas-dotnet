﻿using Desafio.Domain;
using Desafio.Domain.Interface.Repository;
using Desafio.Domain.Interface.Service;

namespace Desafio.Service
{
    public abstract class BaseService : IBaseService
    {
        private readonly IBaseRepository _baseRepository;
        public BaseService(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public IUnitOfWork UnitOfWork => _baseRepository.UnitOfWork;

        public void Dispose()
        {
            _baseRepository.Dispose();
        }
    }
}
