﻿using Desafio.Domain;
using Desafio.Domain.Entity;
using Desafio.Domain.Interface;
using Desafio.Domain.Interface.Repository;
using Desafio.Domain.Interface.Service;
using System;
using System.Threading.Tasks;

namespace Desafio.Service
{
    public class FilmeService : BaseService, IFilmeService
    {
        private readonly IFilmeRepository _filmeRepository;
        private readonly IAspNetUser _user;

        public FilmeService(IFilmeRepository filmeRepository, IAspNetUser user) : base(filmeRepository)
        {
            _filmeRepository = filmeRepository;
            _user = user;
        }

        public async Task Cadastro(Filme filme, string[] Atores, string[] Diretores, string[] Generos)
        {
            foreach (var ator in Atores)
            {
                var atorBancoDados = await _filmeRepository.ObterAtorPorNome(ator);
                if (atorBancoDados != null)
                    filme.AdicionarAtor(atorBancoDados);
                else
                    filme.AdicionarAtor(new Ator(ator));
            }

            foreach (var diretor in Diretores)
            {
                var diretorBancoDados = await _filmeRepository.ObterDiretorPorNome(diretor);

                if (diretorBancoDados != null)
                    filme.AdicionarDiretor(diretorBancoDados);
                else
                    filme.AdicionarDiretor(new Diretor(diretor));
            }

            foreach (var genero in Generos)
            {
                var generoBancoDados = await _filmeRepository.ObterGeneroPorDescricao(genero);

                if (generoBancoDados != null)
                    filme.AdicionarGenero(generoBancoDados);
                else
                    filme.AdicionarGenero(new Genero(genero));
            }


            await _filmeRepository.CadastrarAsync(filme);
            await _filmeRepository.UnitOfWork.Commit();
        }

        public async Task AdicionarVotoFilme(Guid filmeId, int voto)
        {
            var filme = await _filmeRepository.ObterPorIdComVotosAsync(filmeId);
            if (filme == null)
            {
                throw new DomainException("Filme não existe, voto não computado");
            }

            filme.AdicionarVoto(_user.IdUsuario, voto);

            await _filmeRepository.UnitOfWork.Commit();
        }
    }
}
