﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Desafio.Domain.Interface.Service
{
    public interface IBaseService : IDisposable
    {
        IUnitOfWork UnitOfWork { get; }
    }
}
