﻿using Desafio.Domain.Entity;
using System;
using System.Threading.Tasks;

namespace Desafio.Domain.Interface.Service
{
    public interface IFilmeService : IBaseService
    {
        Task Cadastro(Filme filme, string[] Atores, string[] Diretores, string[] Generos);
        Task AdicionarVotoFilme(Guid filmeId, int nota);
    }
}
