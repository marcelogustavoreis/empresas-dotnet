﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Desafio.Domain.Interface
{
    public interface IAspNetUser
    {
        Guid IdUsuario { get; }
    }
}
