﻿using Desafio.Domain.Dto;
using Desafio.Domain.Entity;
using Desafio.Domain.Filtros;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Desafio.Domain.Interface.Repository
{
    public interface IApplicationUserRepository : IBaseRepository
    {
        Task<ListaPaginada<UsuarioAtivoDto>> ListarNaoAdministradoresAtivosAsync(ApplicationUserFiltro filtro);
    }    
}
