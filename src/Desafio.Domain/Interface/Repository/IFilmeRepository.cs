﻿using Desafio.Domain.Dto;
using Desafio.Domain.Entity;
using Desafio.Domain.Filtros;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Desafio.Domain.Interface.Repository
{
    public interface IFilmeRepository : IBaseRepository
    {
        Task<Filme> CadastrarAsync(Filme filme);

        Task<Filme> ObterPorIdComVotosAsync(Guid id);

        Task<ListaPaginada<ListagemFilmeDto>> ListarAsync(FilmeFiltro filtro);
        Task<DetalheFilmeDto> DetalhesAsync(Guid filmeId);

        Task<Ator> ObterAtorPorNome(string nome);
        Task<Diretor> ObterDiretorPorNome(string nome);
        Task<Genero> ObterGeneroPorDescricao(string descricao);
    }
}
