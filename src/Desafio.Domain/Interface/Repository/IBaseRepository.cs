﻿using Desafio.Domain.Entity;
using System;

namespace Desafio.Domain.Interface.Repository
{
    public interface IBaseRepository : IDisposable
    {
        IUnitOfWork UnitOfWork { get; }
    }
}
