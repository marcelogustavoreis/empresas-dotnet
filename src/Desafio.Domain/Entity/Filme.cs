﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Desafio.Domain.Entity
{
    public class Filme : EntityBase
    {
        public string Titulo { get; private set; }
        public string Sinopse { get; private set; }
        public ICollection<Ator> Atores { get; private set; }
        public ICollection<Diretor> Diretores { get; private set; }
        public ICollection<Genero> Generos { get; private set; }
        public ICollection<VotoFilme> Votos { get; private set; }

        public double MediaVotos
        {
            get
            {
                if(Votos == null || !Votos.Any()) return 0;

                return Votos.Sum(x => x.Nota) / Votos.Count();
            }
        }
        public Filme(string titulo, string sinopse)
        {
            Id = Guid.NewGuid();
            Titulo = titulo;
            Sinopse = sinopse;
        }

        protected Filme() { }

        public void AdicionarAtor(Ator ator)
        {
            if (Atores == null) Atores = new List<Ator>();

            if (Atores.Any(x => x.Id == ator.Id))
            {
                throw new DomainException("Ator já está cadastrado para este filme");
            }

            Atores.Add(ator);
        }

        public void AdicionarDiretor(Diretor diretor)
        {
            if (Diretores == null) Diretores = new List<Diretor>();

            if (Diretores.Any(x => x.Id == diretor.Id))
            {
                throw new DomainException("Diretor já está cadastrado para este filme");
            }

            Diretores.Add(diretor);
        }

        public void AdicionarGenero(Genero genero)
        {
            if (Generos == null) Generos = new List<Genero>();

            if (Generos.Any(x => x.Id == genero.Id))
            {
                throw new DomainException("Genero já está cadastrado para este filme");
            }

            Generos.Add(genero);
        }

        public void AdicionarVoto(Guid usuarioId, int voto)
        {
            if (Votos == null) Votos = new List<VotoFilme>();

            var votoUsuario = Votos.FirstOrDefault(x => x.UsuarioId == usuarioId);
            if (votoUsuario != null)
            {
                votoUsuario.AdicionarNota(voto);
            }
            else
            {
                Votos.Add(new VotoFilme(usuarioId, Id, voto));
            }
        }
    }
}
