﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Text;

namespace Desafio.Domain.Entity
{
    public abstract class EntityBase
    {
        public Guid Id { get; protected set; }
    }
}
