﻿using System;
using System.Collections.Generic;

namespace Desafio.Domain.Entity
{
    public class Diretor : EntityBase
    {
        public string Nome { get; private set; }
        public ICollection<Filme> Filmes { get; set; }

        public Diretor(string nome)
        {
            Id = Guid.NewGuid();
            Nome = nome;
        }

        protected Diretor() { }
    }
}
