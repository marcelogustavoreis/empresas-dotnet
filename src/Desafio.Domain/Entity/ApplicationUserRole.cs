﻿using Microsoft.AspNetCore.Identity;
using System;

namespace Desafio.Domain.Entity
{
    public class ApplicationUserRole : IdentityUserRole<Guid>
    {
        public virtual ApplicationRole Role { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
