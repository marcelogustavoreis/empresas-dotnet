﻿using System;
using System.Collections.Generic;

namespace Desafio.Domain.Entity
{
    public class Genero : EntityBase
    {
        public string Descricao { get; private set; }
        public ICollection<Filme> Filmes { get; set; }

        public Genero(string descricao)
        {
            Id = Guid.NewGuid();
            Descricao = descricao;
        }

        protected Genero() { }
    }
}
