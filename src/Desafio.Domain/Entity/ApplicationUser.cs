﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace Desafio.Domain.Entity
{
    public class ApplicationUser : IdentityUser<Guid>
    {
        public string NomeCompleto { get; private set; }
        public bool EstaAtivo { get; private set; }

        public virtual ICollection<ApplicationUserRole> UserRoles { get; set; }

        protected ApplicationUser() { }

        public ApplicationUser(string nomeCompleto, string email)
        {
            NomeCompleto = nomeCompleto;
            Email = email;
            UserName = email;
            EstaAtivo = true;
        }

        public void Desativar()
        {
            this.EstaAtivo = false;
        }

        public void Atualizar(string nomeCompleto)
        {
            if (string.IsNullOrWhiteSpace(nomeCompleto)) throw new DomainException("NomeCompleto não pode ser vazio");
            NomeCompleto = nomeCompleto;
        }
    }
}
