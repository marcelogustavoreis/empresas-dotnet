﻿using System;
using System.Collections.Generic;

namespace Desafio.Domain.Entity
{
    public class Ator : EntityBase
    {
        public string Nome { get; private set; }
        public ICollection<Filme> Filmes { get; set; }

        public Ator(string nome)
        {
            Id = Guid.NewGuid();
            Nome = nome;
        }

        protected Ator() { }
    }
}
