﻿using System;
using System.Collections.Generic;

namespace Desafio.Domain.Entity
{
    public class VotoFilme
    {
        public Guid UsuarioId { get; private set; }
        public ApplicationUser Usuario { get; private set; }
        public Guid FilmeId { get; private set; }
        public Filme Filme { get; private set; }
        public int Nota { get; private set; }

        public VotoFilme(Guid usuarioId, Guid filmeId, int nota)
        {
            UsuarioId = usuarioId;
            FilmeId = filmeId;

            AdicionarNota(nota);
        }

        protected VotoFilme() { }

        public void AdicionarNota(int nota)
        {
            if (nota < 0 || nota > 4)
            {
                throw new DomainException("Nota inválida, inserir valor entre 0 e 4");
            }

            Nota = nota;
        }
    }
}
