﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace Desafio.Domain.Entity
{
    public class ApplicationRole : IdentityRole<Guid>
    {
        public ApplicationRole(string name)
        {
            Name = name;
        }

        public virtual ICollection<ApplicationUserRole> UserRoles { get; set; }
    }
}
