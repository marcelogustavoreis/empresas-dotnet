﻿using System.Text;

namespace Desafio.Domain.Filtros
{
    public class FilmeFiltro : FiltroPaginacaoBase
    {
        public string TituloFilme { get; set; }
        public string Ator { get; set; }
        public string Diretor { get; set; }
        public string Genero { get; set; }
        public FilmeOrdenacao Ordenacao { get; set; }

        public FilmeFiltro()
        {
            QuantidadeItensPagina = 20;
        }
    }

    public enum FilmeOrdenacao
    {
        MediaVotos,
        Titulo
    }
}
