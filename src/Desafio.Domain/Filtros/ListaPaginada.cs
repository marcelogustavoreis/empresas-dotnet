﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Desafio.Domain.Filtros
{
    public class ListaPaginada<T>
    {
        public int PaginaAtual { get; private set; }
        public int QuantidadePaginas { get; private set; }
        public int QuantidadeItens { get; private set; }
        public List<T> Lista { get; private set; }

        public ListaPaginada(List<T> items, int count, int pageIndex, int pageSize)
        {
            PaginaAtual = pageIndex;
            QuantidadePaginas = (int)Math.Ceiling(count / (double)pageSize);
            QuantidadeItens = items.Count;

            Lista = new List<T>();
            Lista.AddRange(items);
        }

        public static async Task<ListaPaginada<T>> CreateAsync(IQueryable<T> source, int pagina, int quantidadeItensPagina)
        {
            var count = await source.CountAsync();
            var items = await source.Skip((pagina - 1) * quantidadeItensPagina).Take(quantidadeItensPagina).ToListAsync();
            return new ListaPaginada<T>(items, count, pagina, quantidadeItensPagina);
        }
    }
}
