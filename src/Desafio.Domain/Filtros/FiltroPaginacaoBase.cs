﻿namespace Desafio.Domain.Filtros
{
    public class FiltroPaginacaoBase
    {
        public int Pagina { get; set; }
        public int QuantidadeItensPagina { get; set; }

        public FiltroPaginacaoBase()
        {
            if (Pagina == 0) Pagina = 1;
            if (QuantidadeItensPagina == 0) QuantidadeItensPagina = 10;
        }
    }
}
