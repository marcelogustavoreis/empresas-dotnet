﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Desafio.Domain
{
    public interface IUnitOfWork
    {
        Task<bool> Commit();
    }
}
