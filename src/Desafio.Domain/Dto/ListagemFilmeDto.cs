﻿using System;

namespace Desafio.Domain.Dto
{
    public class ListagemFilmeDto
    {
        public Guid Id { get; set; }
        public string Titulo { get; set; }
        public string Sinopse { get; set; }

        public string Atores { get; set; }
        public string Diretores { get; set; }
        public string Generos { get; set; }
    }
}
