﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Desafio.Domain.Dto
{
    public class UsuarioAtivoDto
    {
        public string NomeCompleto { get; set; }
        public string Email { get; set; }
    }
}
